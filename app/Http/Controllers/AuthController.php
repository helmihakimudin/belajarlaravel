<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register(){
    	return view('register');
    }

    public function welcome(Request $request){
    	$nama1 = $request->firstname;
    	$nama2 = $request->lastname;
    	
    	return view('welcome', compact('nama1', 'nama2'));
    }
}

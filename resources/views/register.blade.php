<!DOCTYPE html>
<html>
<head>
	<title>Form SanberBook</title>
</head>
<body style="background: yellow;">

	<h2>Buat Account Baru!</h2>
	<h3>Sign Up Form</h3>

	<form action="welcome">
		@csrf
		<label for="firstname">First Name:</label><br><br>
		<input type="text" placeholder="First Name" value="" id="firstname" name="firstname">
		<br><br>
		<label for="lastname">Last Name:</label><br><br>
		<input type="text" placeholder="Last Name" value="" id="lastname" name="lastname">
		<br><br>

		<label>Gender:</label><br><br>
		<input type="radio" name="Gender" value="0">Male <br>
		<input type="radio" name="Gender" value="1">Female <br>
		<input type="radio" name="Gender" value="2">Other <br>

		<br>
		<label>Nationality:</label><br><br>
		<select>
			<option value="indonesia">Indonesia</option>
			<option value="singapore">Singapore</option>
			<option value="malaysia">Malaysia</option>
			<option value="australia">Australia</option>
		</select><br><br>

		<label>Language Spoken:</label><br><br>
		<input type="checkbox" name="language" value="0">Bahasa Indonesia <br>
		<input type="checkbox" name="language" value="1">English <br>
		<input type="checkbox" name="language" value="2">Other <br>
		<br>
		<label>Bio:</label><br><br>
		<textarea cols="30" rows="7" id="bio"></textarea>
		<br>
		
		<button type="submit" value="Sign Up">Sign Up</button>

	</form>


</body>
</html>